/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snoopy;

import java.io.Serializable;

/**
 *
 * @author Richard
 */
public class Partie implements Serializable {
    private char [][] plateau;
    private boolean gameover;
    private int nv1timer;
    private int nv2timer;
    private int nv3timer;
    private int currentlevel;
    private boolean pause=false;
    private Snoopy currentperso;
    private Balle []ennemis;
    private int score;
    private int pv;
    
    /**
     * Constructeurs
     * @param plateau niveau à charger
     * @param gameover boolean pour savoir si la partie est finie
     * @param nv1timer premier timer initialisé à 60
     * @param nv2timer deuxieme timer initialisé à 60
     * @param nv3timer troisieme timer initialisé à 60
     * @param currentlevel niveau actuel de la partie
     * @param currentperso personnage snoopy
     * @param ennemis les ennemis sur le terrain
     * @param score score du joueur
     */
    public Partie(char[][] plateau, boolean gameover, int nv1timer, int nv2timer, int nv3timer, int currentlevel, Snoopy currentperso, Balle[] ennemis,int score) 
    
    {
        this.plateau = plateau;
        this.gameover = gameover;
        this.nv1timer = nv1timer;
        this.nv2timer = nv2timer;
        this.nv3timer = nv3timer;
        this.currentlevel = currentlevel;
        this.currentperso = currentperso;
        this.ennemis = ennemis;
        this.score=score;
    }


    /**
     * @return the plateau
     */
    public char[][] getPlateau() {
        return plateau;
    }

    /**
     * @param plateau the plateau to set
     */
    public void setPlateau(char[][] plateau) 
    {
        this.setPlateau(plateau);
    }
    
    
    
    /**
     * @return the gameover
     */
    public boolean isGameover() {
        return gameover;
    }

    @Override
    public String toString() {
        return "Partie{" + "plateau=" + plateau + ", gameover=" + gameover + ", nv1timer=" + nv1timer + ", nv2timer=" + nv2timer + ", nv3timer=" + nv3timer + ", currentlevel=" + currentlevel + ", currentperso=" + currentperso + ", ennemis=" + ennemis + '}';
    }

    /**
     * @param gameover the gameover to set
     */
    public void setGameover(boolean gameover) {
        this.gameover = gameover;
    }

   
    /**
     * @return the nv1timer
     */
    public int getNv1timer() {
        return nv1timer;
    }

    /**
     * @param nv1timer the nv1timer to set
     */
    public void setNv1timer(int nv1timer) {
        this.nv1timer = nv1timer;
    }

    /**
     * @return the nv2timer
     */
    public int getNv2timer() {
        return nv2timer;
    }

    /**
     * @param nv2timer the nv2timer to set
     */
    public void setNv2timer(int nv2timer) {
        this.nv2timer = nv2timer;
    }

    /**
     * @return the nv3timer
     */
    public int getNv3timer() {
        return nv3timer;
    }

    /**
     * @param nv3timer the nv3timmer to set
     */
    public void setNv3timer(int nv3timer) {
        this.nv3timer = nv3timer;
    }

    /**
     * @return the currentlevel
     */
    public int getCurrentlevel() {
        return currentlevel;
    }

    /**
     * @param currentlevel the currentlevel to set
     */
    public void setCurrentlevel(int currentlevel) {
        this.currentlevel = currentlevel;
    }

    /**
     * @return the currentperso
     */
    public Snoopy getCurrentperso() {
        return currentperso;
    }

    /**
     * @param currentperso the currentperso to set
     */
    public void setCurrentperso(Snoopy currentperso) {
        this.currentperso = currentperso;
    }

    /**
     * @return the ennemis
     */
    public Balle[] getEnnemis() {
        return ennemis;
    }

    /**
     * @param ennemis the ennemis to set
     */
    public void setEnnemis(Balle[] ennemis) {
        this.ennemis = ennemis;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * @return the pv
     */
    public int getPv() {
        return pv;
    }

    /**
     * @param pv the pv to set
     */
    public void setPv(int pv) {
        this.pv = pv;
    }

    /**
     * @return the pause
     */
    public boolean getPause() {
        return pause;
    }

    /**
     * @param pause the pause to set
     */
    public void setPause(boolean pause) {
        this.pause = pause;
    }
    
    
}
