/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snoopy;

import java.io.Serializable;

/**
 *
 * @author Richard
 */
public class Entity implements Serializable {
    private int posx;
    private int posy;

    /**
     * Consctruteur null si pas de paramètre
     */
    public Entity ()
    {
        this.posx=0;
        this.posy=0;
    }
    /**
     * Constructeur surchargé
     * @param tx posx de l'entité
     * @param ty posy de l'entité
     */
    public Entity (int tx, int ty)
    {
        this.posx=tx;
        this.posy=ty;
    }
    /**
     * @return the pox
     */
    public int getPosx() {
        return posx;
    }

    /**
     * @param posx the pox to set
     */
    public void setPosx(int posx) {
        this.posx = posx;
    }

    /**
     * @return the posy
     */
    public int getPosy() {
        return posy;
    }

    /**
     * @param posy the posy to set
     */
    public void setPosy(int posy) {
        this.posy = posy;
    }
}