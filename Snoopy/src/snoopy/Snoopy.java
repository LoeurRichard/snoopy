/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snoopy;

import java.io.Serializable;
import java.util.Scanner;

/**
 *
 * @author Richard
 */
public class Snoopy extends Entity implements Serializable{
    private int pv;
    private transient Scanner sc = new Scanner(System.in);
    private char [][]plateau;
    private Snoopy perso_principal;
    private Entity bloc;
    private String nom;
    private int niveau;
    private int fin=0;
    private int nombre=2;
    private int level=0;
    

    /**
     * Constructeur héritant de Entity
     */
    public Snoopy ()
    {
        super();
    }
    /**
     * 
     * @param tx posx de Snoopy
     * @param ty posy de Snoopy
     * @param tpv PV de Snoopy
     * @param board plateau de jeu
     * @param tniveau niveau 
     * @param tfin boolean pour savoir si le jeu est terminé ou non
     */
    public Snoopy (int tx,int ty, int tpv, char[][] board,int tniveau,int tfin)
    {
        super(tx,ty);
        this.pv=tpv;
        plateau=board;
        this.niveau=tniveau;
        this.fin=tfin;
    }
    /**
     * @return the pv
     */
    public int getPv() {
        return pv;
    }

    /**
     * @param pv the pv to set
     */
    public void setPv(int pv) {
        this.pv = pv;
    }
    /**
     * Déplacement de Snoopy et blindages 
     * @param quit saisie de l'utilisateur
     * @param terrain plateau de jeu
     * @param perso Snoopy
     */
    public void deplacer(char quit,char [][] terrain, Snoopy perso)
    {
        switch(quit)
        {
            case 'd' :
                if (terrain[perso.getPosx()][perso.getPosy()+1] == '#' || terrain[perso.getPosx()][perso.getPosy()+1] == 'M' || terrain[perso.getPosx()][perso.getPosy()+1] == 'C' )
                  {
                      
                  }
                 else if (terrain[perso.getPosx()][perso.getPosy()+1] == 'P')
                 {
                     if (terrain[perso.getPosx()][perso.getPosy()+2] == 'P' || terrain[perso.getPosx()][perso.getPosy()+2] == 'M' || terrain[perso.getPosx()][perso.getPosy()+2] == 'C' || terrain[perso.getPosx()][perso.getPosy()+2] == '#'|| terrain[perso.getPosx()][perso.getPosy()+2] == 'T'|| terrain[perso.getPosx()][perso.getPosy()+2] == 'O'|| terrain[perso.getPosx()][perso.getPosy()+2] == 'B')
                     {
                         
                     }
                     else 
                     {
                        terrain[perso.getPosx()][perso.getPosy()] = '-';
                        perso.setPosy(perso.getPosy()+1);  
                        terrain[perso.getPosx()][perso.getPosy()+1] = 'M';   
                     }
                 }
                  else if (terrain[perso.getPosx()][perso.getPosy()+1] == 'O')
                 {
                     terrain[perso.getPosx()][perso.getPosy()] = '-';
                     perso.setPosy(perso.getPosy()+1);
                     perso.setNiveau(perso.getNiveau() + 1);
                 }
                  else if (terrain[perso.getPosx()][perso.getPosy()+1]=='T')
                  {
                     terrain[perso.getPosx()][perso.getPosy()] = '-';
                     perso.setPosy(perso.getPosy()+1);
                     perso.setPv(0);
                  }
                 else
                  {
                     terrain[perso.getPosx()][perso.getPosy()]='-';
                     perso.setPosy(perso.getPosy()+1);
                     
                  }
                break;

            case 'q' :
                {
                  if (terrain[perso.getPosx()][perso.getPosy()-1] == '#' || terrain[perso.getPosx()][perso.getPosy()-1] == 'M' || terrain[perso.getPosx()][perso.getPosy()-1] == 'C'|| terrain[perso.getPosx()][perso.getPosy()-1] == 'T')
                  {
                      
                  }

                 else if (terrain[perso.getPosx()][perso.getPosy()-1] == 'P')
                 {
                     
                     if (terrain[perso.getPosx()][perso.getPosy()-2] == 'P' || terrain[perso.getPosx()][perso.getPosy()-2] == 'M' || terrain[perso.getPosx()][perso.getPosy()-2] == 'C' || terrain[perso.getPosx()][perso.getPosy()-2] == '#'|| terrain[perso.getPosx()][perso.getPosy()-2] == 'O'|| terrain[perso.getPosx()][perso.getPosy()-2] == 'B')
                     {
                         
                     }
                     else
                     {
                        terrain[perso.getPosx()][perso.getPosy()] = '-';
                        perso.setPosy(perso.getPosy()-1);  
                        terrain[perso.getPosx()][perso.getPosy()-1] = 'M'; 
                     }
                 }
                 else if (terrain[perso.getPosx()][perso.getPosy()-1]=='T')
                  {
                     terrain[perso.getPosx()][perso.getPosy()] = '-';
                     perso.setPosy(perso.getPosy()-1);
                     perso.setPv(0);
                  }
                 
                 else if (terrain[perso.getPosx()][perso.getPosy()-1] == 'O')
                 {
                     terrain[perso.getPosx()][perso.getPosy()] = '-';
                     perso.setPosy(perso.getPosy()-1); 
                     perso.setNiveau(perso.getNiveau() + 1);
                 }
                  else
                  {
                     terrain[perso.getPosx()][perso.getPosy()]='-';
                     perso.setPosy(perso.getPosy()-1);
                  }   
                  break;
                }
                
               case 's':
                {
                 if (terrain[perso.getPosx()+1][perso.getPosy()] == '#' || terrain[perso.getPosx()+1][perso.getPosy()] == 'M' || terrain[perso.getPosx()+1][perso.getPosy()] == 'C'|| terrain[perso.getPosx()+1][perso.getPosy()] == 'T')
                  {
                      
                  }
                 else if (terrain[perso.getPosx()+1][perso.getPosy()] == 'P')
                 {
                     if (terrain[perso.getPosx()+2][perso.getPosy()] == 'P' || terrain[perso.getPosx()+2][perso.getPosy()] == 'M' || terrain[perso.getPosx()+2][perso.getPosy()] == 'C' || terrain[perso.getPosx()+2][perso.getPosy()] == '#'|| terrain[perso.getPosx()+2][perso.getPosy()] == 'O'|| terrain[perso.getPosx()+2][perso.getPosy()] == 'B')
                     {
                         
                     }
                     else
                     {
                        terrain[perso.getPosx()][perso.getPosy()] = '-';
                        perso.setPosx(perso.getPosx()+1);  
                        terrain[perso.getPosx()+1][perso.getPosy()] = 'M'; 
                     }
                 }
                 else if (terrain[perso.getPosx()+1][perso.getPosy()] == 'O')
                 {
                     terrain[perso.getPosx()][perso.getPosy()] = '-';
                     perso.setPosx(perso.getPosx()+1);  
                     perso.setNiveau(perso.getNiveau() + 1);            
                 }
                 else if (terrain[perso.getPosx()+1][perso.getPosy()]=='T')
                  {
                     terrain[perso.getPosx()][perso.getPosy()] = '-';
                     perso.setPosy(perso.getPosx()+1);
                     perso.setPv(0);
                  }
                 else
                 {
                    terrain[perso.getPosx()][perso.getPosy()]='-';
                    perso.setPosx(perso.getPosx()+1);
                 }
                 break;
                }
               case 'z':
                {
                 if (terrain[perso.getPosx()-1][perso.getPosy()] == '#' || terrain[perso.getPosx()-1][perso.getPosy()] == 'M' || terrain[perso.getPosx()-1][perso.getPosy()] == 'C'|| terrain[perso.getPosx()-1][perso.getPosy()] == 'T')
                  {
                      
                  }
                 else if (terrain[perso.getPosx()-1][perso.getPosy()] == 'P')
                 {
                     if (terrain[perso.getPosx()-2][perso.getPosy()] == 'P' || terrain[perso.getPosx()-2][perso.getPosy()] == 'M' || terrain[perso.getPosx()-2][perso.getPosy()] == 'C' || terrain[perso.getPosx()-2][perso.getPosy()] == '#'|| terrain[perso.getPosx()-2][perso.getPosy()] == 'O'|| terrain[perso.getPosx()-2][perso.getPosy()] == 'B')
                     {
                         
                     }
                     else 
                     {
                     terrain[perso.getPosx()][perso.getPosy()] = '-';
                     perso.setPosx(perso.getPosx()-1);  
                     terrain[perso.getPosx()-1][perso.getPosy()] = 'M'; 
                     }
                 }
                 else if (terrain[perso.getPosx()-1][perso.getPosy()]=='T')
                  {
                     terrain[perso.getPosx()][perso.getPosy()] = '-';
                     perso.setPosy(perso.getPosx()-1);
                     perso.setPv(0);
                  }
                 else if (terrain[perso.getPosx()-1][perso.getPosy()] == 'O')
                 {
                     terrain[perso.getPosx()][perso.getPosy()] = '-';
                     perso.setPosx(perso.getPosx()-1);  
                     perso.setNiveau(perso.getNiveau() + 1);            
                 }
                 else
                 {
                     terrain[perso.getPosx()][perso.getPosy()]='-';
                     perso.setPosx(perso.getPosx()-1);
                 }
                 break;
                }
               case 'a' :
                   this.casser(quit,terrain,perso);
                   break;
               case 'p' :
                   break;
               default : 
                   System.out.println("Erreur de saisie");
                   break;
        }
        }
    /**
     * Méthode pour casser les obstacles
     * @param quit saisie 
     * @param terrain plateau
     * @param perso Snoopy
     */
    public void casser(char quit,char [][] terrain, Snoopy perso)
    {
        if (terrain[perso.getPosx()][perso.getPosy()+1] == 'C')
        {
            terrain[perso.getPosx()][perso.getPosy()+1] = '-';
        }
        if (terrain[perso.getPosx()][perso.getPosy()-1] == 'C')
        {
            terrain[perso.getPosx()][perso.getPosy()-1] = '-';
        }
        if (terrain[perso.getPosx()+1][perso.getPosy()] == 'C')
        {
            terrain[perso.getPosx()+1][perso.getPosy()] = '-';
        }
        if (terrain[perso.getPosx()-1][perso.getPosy()] == 'C')
        {
            terrain[perso.getPosx()-1][perso.getPosy()] = '-';
        }

    }

    /**
     * @return the sc
     */
    public Scanner getSc() {
        return sc;
    }

    /**
     * @param sc the sc to set
     */
    public void setSc(Scanner sc) {
        this.sc = sc;
    }

    /**
     * @return the plateau
     */
    public char[][] getPlateau() {
        return plateau;
    }

    /**
     * @param plateau the plateau to set
     */
    public void setPlateau(char[][] plateau) {
        this.plateau = plateau;
    }

    /**
     * @return the perso_principal
     */
    public Snoopy getPerso_principal() {
        return perso_principal;
    }

    /**
     * @param perso_principal the perso_principal to set
     */
    public void setPerso_principal(Snoopy perso_principal) {
        this.perso_principal = perso_principal;
    }

    /**
     * @return the bloc
     */
    public Entity getBloc() {
        return bloc;
    }

    /**
     * @param bloc the bloc to set
     */
    public void setBloc(Entity bloc) {
        this.bloc = bloc;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the niveau
     */
    public int getNiveau() {
        return niveau;
    }

    /**
     * @param niveau the niveau to set
     */
    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    /**
     * @return the fin
     */
    public int getFin() {
        return fin;
    }

    /**
     * @param fin the fin to set
     */
    public void setFin(int fin) {
        this.fin = fin;
    }

    /**
     * @return the nombre
     */
    public int getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(int level) {
        this.level = level;
    }

 
    
}
