/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snoopy;

import java.awt.AWTException;
import  java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Scanner;
import javax.swing.JTextField;
/**
 *
 * @author John
 */
public class Menu {
    
    Scanner sc = new Scanner(System.in);
    private int choix=0;
    Partie partie;
    Balle[] tballe;
    Graphisme graphisme1=new Graphisme();
    
    /**
     * Affichage du menu avec le choix de l'utilisateur 
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public void accueil() throws IOException, ClassNotFoundException
    {
        
        choix=0;
    
    while(choix!=5)
        {
            System.out.println("Menu");
            System.out.println("1. Jouer au jeu");
            System.out.println("2. Règles");
            System.out.println("3. Charger une partie");
            System.out.println("4. Mot de passe");
            System.out.println("5. Quitter");
            choix=sc.nextInt();
            sc.nextLine();
            switch(choix)
            {
                case 1:
                    choix=5;

         
         char [][]plateaudejeu=graphisme1.recupNiveau(1);
         Snoopy perso=new Snoopy(4,8,3,plateaudejeu,0,0);
         Balle[] ennemis=new Balle [perso.getNombre()];
         ennemis[0]= new Balle(8,7,4);
         ennemis[1]= new Balle(8,16,3);
         Partie currentgame=new Partie (plateaudejeu,false,60,60,60,1,perso,ennemis,0);
         Thread demandedeplacement = new Thread(new Moteur(plateaudejeu,perso,ennemis,currentgame));
         graphisme1.affichage(plateaudejeu,ennemis,perso,currentgame);
       
         demandedeplacement.start();
                    break;
                    
                case 2:
                    System.out.println("Ramassez des oiseaux");
                    break;
                    
                case 3:
            String sauvegarde=sc.nextLine();
            File fichier =  new File(sauvegarde+".txt") ;
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichier)) ;
            Partie save=(Partie)ois.readObject();
            System.out.println(save);
            choix=5;
            
             int s_x=0,s_y=0;
                 int[] tb_x;
                 tb_x = new int [save.getEnnemis().length];
                 int[] tb_y;
                 tb_y = new int [save.getEnnemis().length];
                 int cpt_balle=0;
            for(int i = 0;i<10;i++)
                {
                    for(int j = 0;j<20;j++)
                    {
                        if (save.getPlateau()[i][j]=='S')
                        {
                            s_x=i;
                            s_y=j;
                            
                        }
                         if (save.getPlateau()[i][j]=='B')
                        {
                            tb_x[cpt_balle]=i;
                            tb_y[cpt_balle]=j;
                            cpt_balle++;
                            
                        }
                    }         
                }  
                Graphisme graphisme1=new Graphisme();
            char [][]plateaudejeu4=save.getPlateau();
            Snoopy perso4=new Snoopy(s_x,s_y,save.getPv(),plateaudejeu4,0,0);
            int nbre_ennemis=save.getEnnemis().length;
            Balle[] ennemis4=new Balle [nbre_ennemis];
            for (int i=0;i<nbre_ennemis;i++)
            {

                ennemis4[i]=new Balle (tb_x[i],tb_y[i],nbre_ennemis);

            }
            Partie currentgame4=new Partie (plateaudejeu4,false,save.getNv1timer(),save.getNv2timer(),save.getNv3timer(),save.getCurrentlevel(),perso4,ennemis4,save.getScore());
            Thread demandedeplacement4 = new Thread(new Moteur(plateaudejeu4,perso4,ennemis4,currentgame4));
            graphisme1.affichage(plateaudejeu4,ennemis4,perso4,currentgame4);
            demandedeplacement4.start();    
         
        
                    break;
                case 4:
                    
                    choix = 5;
                    int numero = sc.nextInt();
                    if(numero==2)
                        {
                            Graphisme graphisme2=new Graphisme();
                            char [][]plateaudejeu2=graphisme2.recupNiveau(2);
                            Snoopy perso2=new Snoopy(6,5,3,plateaudejeu2,0,0);
                            //int nbre_ennemis=1;
                            perso2.setLevel(2);
                            Balle[] ennemis2=new Balle [perso2.getNombre()];
                            ennemis2[0]= new Balle (1,7,4);
                            ennemis2[1]= new Balle (8,15,2);
                            Partie currentgame2=new Partie (plateaudejeu2,false,60,60,60,1,perso2,ennemis2,0);
                            Thread demandedeplacement2 = new Thread(new Moteur(plateaudejeu2,perso2,ennemis2,currentgame2));
                            graphisme2.affichage(plateaudejeu2,ennemis2,perso2,currentgame2);
                            demandedeplacement2.start();
                        }
                    if(numero==3)
                        {
                            Graphisme graphisme3=new Graphisme();
                            char [][]plateaudejeu3=graphisme3.recupNiveau(3);
                            Snoopy perso3=new Snoopy(5,7,3,plateaudejeu3,0,0);
                            perso3.setLevel(4);
                            Balle[] ennemis3=new Balle [perso3.getNombre()];
                            ennemis3[0]= new Balle (8,3,3);
                            ennemis3[1]= new Balle (8,15,2);
                            Partie currentgame3=new Partie (plateaudejeu3,false,60,60,60,1,perso3,ennemis3,0);
                            Thread demandedeplacement3 = new Thread(new Moteur(plateaudejeu3,perso3,ennemis3,currentgame3));
                            graphisme3.affichage(plateaudejeu3,ennemis3,perso3,currentgame3);
                            demandedeplacement3.start();
                        }
                    break;
                case 5:
                    System.exit(0);
                    break;
            }
        }
    }

    /**
     * @return the choix
     */
    public int getchoix() {
        return choix;
    }

    /**
     * @param choix the choix to set
     */
    public void setchoix(int choix) {
        this.choix = choix;
    }
    
}
