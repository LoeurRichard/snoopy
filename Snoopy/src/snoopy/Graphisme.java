/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snoopy;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import static java.util.concurrent.TimeUnit.SECONDS;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Richard
 */
public class Graphisme {
    public String nom;
     private final ScheduledExecutorService scheduler =
       Executors.newScheduledThreadPool(1);
     
     /**
      * Méthode pour charger le niveau et l'afficher
      * @param niveau pour savoir quel niveau charger
      * @return le plateau du niveau
      * @throws IOException 
      */
    public char[][] recupNiveau (int niveau) throws IOException
    {
         FileReader lecteur=null; 
    //FileWriter ecrivain; 
        char [][] plateau = new char[10][20];
    char c; 
        try { 
            switch (niveau)
                    {
            case 1 : 
                lecteur = new FileReader("Niveau1.txt");
                break;
            case 2 : 
                lecteur = new FileReader("Niveau2.txt");
                break;
            case 3 : 
                lecteur = new FileReader("Niveau3.txt");
                break;
                    
                
            }
                    
            } catch (FileNotFoundException ex) 
                {
            Logger.getLogger(Moteur.class.getName()).log(Level.SEVERE, null, ex);
                }
        for(int i = 0;i<10;i++)
    {
        for(int j = 0;j<20;j++)
         {
            c= (char) lecteur.read();
            plateau[i][j]=c;
            System.out.print(plateau[i][j]);
         }
        System.out.println("");
    }
        return plateau;
        
    }
    /**
     * Méthode pour afficher le terrain, le timer, le score, la vie, le plateau, les balles toutes les X secondes
     * @param plateau le terrain de jeu
     * @param ennemis les balles
     * @param perso Snoopy
     * @param currentgame la partie en cours
     * @throws IOException 
     */
    public void affichage(char [][] plateau, Balle[] ennemis,Snoopy perso,Partie currentgame) throws IOException
    {

        final Runnable affichage;
        
        affichage = new Runnable() {
            public void run()   {
                        if (!currentgame.getPause())
        {
                
                for (int i = 0; i < 50; ++i) System.out.println();
                
                
                char[] [] temp= new char [10][20];
                for(int i = 0;i<10;i++)
                {
                    for(int j = 0;j<20;j++)
                    {
                        temp[i][j]=plateau[i][j];
                        
                    }
                }
                // changement de position desballes
                
                for(int i = 0;i<10;i++)
                {
                    for(int j = 0;j<20;j++)
                    {
                        if (plateau[i][j]=='B')
                        {
                            // boucle for pour retrouver l'ennemis correspondant aux coordonnées
                            for (int k=0;k<ennemis.length;k++)
                            {
                                if (i==ennemis[k].getPosx() && j==ennemis[k].getPosy())
                                {
                                    switch (ennemis[k].getOrientation())
                                    {
                                        case 1 :
                                            // direction gauche bas
                                            // cas ou la case du mur du bas
                                            if (plateau[i+1][j]=='#' )
                                                {
                                                    
                                                    ennemis[k].setOrientation(2);
                                                    ennemis[k].setPosx(i-1);
                                                     ennemis[k].setPosy(j+1);
                                                     temp[i][j]='-';
                                                     temp[i-1][j+1]='B';
                                                     
                                                  // cas du deplacement normal
                                                }else  if (plateau[i+1][j+1]=='-')
                                                {
                                   
                                                    ennemis[k].setPosx(i+1);
                                                    ennemis[k].setPosy(j+1);
                                                    temp[i][j]='-';
                                                    temp[i+1][j+1]='B';
                                                    
                                                }// cas du mur de droite
                                                else if (plateau[i][j+1]=='#' && plateau[i+1][j]=='-' )
                                                {
                                                    ennemis[k].setOrientation(4);
                                                    ennemis[k].setPosx(i+1);
                                                     ennemis[k].setPosy(j-1);
                                                     temp[i][j]='-';
                                                     temp[i+1][j-1]='B';
                                                }
                                                else if(plateau[i+1][j+1]=='#'&&plateau[i+1][j-1]=='-')
                                                {
                                                    ennemis[k].setOrientation(4);
                                                    ennemis[k].setPosx(i+1);
                                                    ennemis[k].setPosy(j-1);
                                                    temp[i][j]='-';
                                                    temp[i+1][j-1]='B';
                                                }
                                                
                                                // cas du coin
                                                else if ((plateau[i+1][j+1] == '#' && plateau[i][j+1] == '#' && plateau[i+1][j]=='#')||(plateau[i+1][j+1]=='O'))
                                                {
                                                    ennemis[k].setOrientation(3);
                                                    ennemis[k].setPosx(i-1);
                                                    ennemis[k].setPosy(j-1);
                                                    temp[i][j]='-';
                                                    temp[i-1][j-1]='B';
                                                }
                                                
                                                // en cas de collision avec snoopy ou un mur
                                                else if (plateau [i+1][j+1]=='S' || plateau [i+1][j+1]=='P' || plateau [i+1][j+1]=='M' ||plateau [i+1][j+1]=='T'||plateau [i+1][j+1]=='C'||plateau [i+1][j+1]=='B')
                                            {
                                                ennemis[k].setOrientation(3);
                                                    ennemis[k].setPosx(i-1);
                                                    ennemis[k].setPosy(j-1);
                                                    temp[i][j]='-';
                                                    temp[i-1][j-1]='B';
                                                    if (plateau [i+1][j+1]=='S')
                                                        perso.setPv(perso.getPv()-1);
                                                    
                                            }
                                                // cas de bloquage
                                                else 
                                                {
                                                    ennemis[k].setPosx(i);
                                                    ennemis[k].setPosy(j);
                                            ennemis[k].setOrientation(ennemis[k].getOrientation());
                                                    temp[i][j]='B';
                                                }
                                        
                                               
                                            break;
                                           
                                        case 2 : 
                                            // direction haut droite
                                            // cas ou la case du mur du haut
                                            if (plateau[i-1][j]=='#' )
                                                {
                                                    
                                                    ennemis[k].setOrientation(1);
                                                    ennemis[k].setPosx(i+1);
                                                     ennemis[k].setPosy(j+1);
                                                     temp[i][j]='-';
                                                     temp[i+1][j+1]='B';
                                                     
                                                  // cas du deplacement normal
                                                }else  if (plateau[i-1][j+1]=='-')
                                        
                                            
                                                {
                                   
                                                    ennemis[k].setPosx(i-1);
                                                    ennemis[k].setPosy(j+1);
                                                    temp[i][j]='-';
                                                    temp[i-1][j+1]='B';
                                                    
                                                }// cas du mur de droite
                                                else if (plateau[i][j+1]=='#' && plateau[i-1][j]=='-' )
                                                {
                                                    ennemis[k].setOrientation(3);
                                                    ennemis[k].setPosx(i-1);
                                                     ennemis[k].setPosy(j-1);
                                                     temp[i][j]='-';
                                                     temp[i-1][j-1]='B';
                                                }
                                                // cas du coin
                                                else if ((plateau[i-1][j+1] == '#' && plateau[i][j+1] == '#' && plateau[i-1][j]=='#')|| plateau[i-1][j+1]=='O')
                                                {
                                                    ennemis[k].setOrientation(4);
                                                    ennemis[k].setPosx(i+1);
                                                    ennemis[k].setPosy(j-1);
                                                    temp[i][j]='-';
                                                    temp[i+1][j-1]='B';
                                                }
                                                else if(plateau[i-1][j+1]=='#'&&plateau[i-1][j-1]=='-')
                                                {
                                                    ennemis[k].setOrientation(3);
                                                    ennemis[k].setPosx(i-1);
                                                    ennemis[k].setPosy(j-1);
                                                    temp[i][j]='-';
                                                    temp[i-1][j-1]='B';
                                                }
                                                // en cas de collision avec snoopy ou un mur
                                                else if (plateau [i-1][j+1]=='S' || plateau [i-1][j+1]=='P'|| plateau [i-1][j+1]=='M'|| plateau [i-1][j+1]=='T'|| plateau [i-1][j+1]=='C'|| plateau [i-1][j+1]=='B')
                                            {
                                                ennemis[k].setOrientation(4);
                                                    ennemis[k].setPosx(i+1);
                                                    ennemis[k].setPosy(j-1);
                                                    temp[i][j]='-';
                                                    temp[i+1][j-1]='B';
                                                    if (plateau [i-1][j+1]=='S')
                                                        perso.setPv(perso.getPv()-1);
                                                    
                                            }
                                                
                                                // cas de bloquage
                                                else 
                                                {
                                                    ennemis[k].setPosx(i);
                                                    ennemis[k].setPosy(j);
                                            ennemis[k].setOrientation(ennemis[k].getOrientation());
                                                    temp[i][j]='B';
                                                }
                                        
                                               
                                            break;
                                        case 3 : // direction haut gauche
                                            // cas ou la case du mur du haut
                                            if (plateau[i-1][j]=='#' )
                                                {                                                   
                                                    ennemis[k].setOrientation(4);
                                                    ennemis[k].setPosx(i+1);
                                                     ennemis[k].setPosy(j-1);
                                                     temp[i][j]='-';
                                                     temp[i+1][j-1]='B';
                                                     
                                                  // cas du deplacement normal
                                                }else  if (plateau[i-1][j-1]=='-')                                          
                                                {                            
                                                    ennemis[k].setPosx(i-1);
                                                    ennemis[k].setPosy(j-1);
                                                    temp[i][j]='-';
                                                    temp[i-1][j-1]='B';
                                                    
                                                }// cas du mur de gauche
                                                else if (plateau[i][j-1]=='#' && plateau[i-1][j]=='-' )
                                                {
                                                    ennemis[k].setOrientation(2);
                                                    ennemis[k].setPosx(i-1);
                                                     ennemis[k].setPosy(j+1);
                                                     temp[i][j]='-';
                                                     temp[i-1][j+1]='B';
                                                }
                                                // cas du coin
                                                else if ((plateau[i-1][j-1] == '#' && plateau[i][j-1] == '#' && plateau[i-1][j]=='#')|| plateau[i-1][j-1]=='O')
                                                {
                                                    ennemis[k].setOrientation(1);
                                                    ennemis[k].setPosx(i+1);
                                                    ennemis[k].setPosy(j+1);
                                                    temp[i][j]='-';
                                                    temp[i+1][j+1]='B';
                                                }
                                                else if(plateau[i-1][j-1]=='#'&&plateau[i-1][j+1]=='-')
                                                {
                                                    ennemis[k].setOrientation(2);
                                                    ennemis[k].setPosx(i-1);
                                                    ennemis[k].setPosy(j+1);
                                                    temp[i][j]='-';
                                                    temp[i-1][j+1]='B';
                                                }
                                                // en cas de collision avec snoopy ou un mur
                                                else if (plateau [i-1][j-1]=='S' || plateau [i-1][j-1]=='P'|| plateau [i-1][j-1]=='M'|| plateau [i-1][j-1]=='T'|| plateau [i-1][j-1]=='C'|| plateau [i-1][j-1]=='B')
                                            {
                                                ennemis[k].setOrientation(1);
                                                    ennemis[k].setPosx(i+1);
                                                    ennemis[k].setPosy(j+1);
                                                    temp[i][j]='-';
                                                    temp[i+1][j+1]='B';
                                                    if (plateau [i-1][j-1]=='S')
                                                     perso.setPv(perso.getPv()-1);                                                   
                                            }
                                                // cas de bloquage
                                                else 
                                                {
                                                    ennemis[k].setPosx(i);
                                                    ennemis[k].setPosy(j);
                                                    ennemis[k].setOrientation(ennemis[k].getOrientation());
                                                    temp[i][j]='B';
                                                }                                                                                       
                                            break;                                           
                                            case 4 : // direction bas gauche
                                            // cas ou la case du mur du bas
                                            if (plateau[i+1][j]=='#')
                                                {                              
                                                    ennemis[k].setOrientation(3);
                                                    ennemis[k].setPosx(i-1);
                                                    ennemis[k].setPosy(j-1);
                                                    temp[i][j]='-';
                                                    temp[i-1][j-1]='B';
                                                     
                                                  // cas du deplacement normal
                                                }else  if (plateau[i+1][j-1]=='-')                                                                                
                                                {                                 
                                                    ennemis[k].setPosx(i+1);
                                                    ennemis[k].setPosy(j-1);
                                                    temp[i][j]='-';
                                                    temp[i+1][j-1]='B';
                                                    
                                                }// cas du mur de gauche
                                                else if (plateau[i][j-1]=='#' && plateau[i+1][j]=='-' )
                                                {
                                                    ennemis[k].setOrientation(1);
                                                    ennemis[k].setPosx(i+1);
                                                    ennemis[k].setPosy(j+1);
                                                    temp[i][j]='-';
                                                    temp[i+1][j+1]='B';
                                                }
                                                // cas du coin
                                                else if ((plateau[i+1][j-1] == '#' && plateau[i][j-1] == '#' && plateau[i+1][j]=='#')|| plateau[i+1][j+1]=='O')
                                                {
                                                    ennemis[k].setOrientation(2);
                                                    ennemis[k].setPosx(i-1);
                                                    ennemis[k].setPosy(j+1);
                                                    temp[i][j]='-';
                                                    temp[i-1][j+1]='B';
                                                }
                                                else if(plateau[i+1][j-1]=='#'&&plateau[i+1][j+1]=='-')
                                                {
                                                    ennemis[k].setOrientation(1);
                                                    ennemis[k].setPosx(i+1);
                                                    ennemis[k].setPosy(j+1);
                                                    temp[i][j]='-';
                                                    temp[i+1][j+1]='B';
                                                }
                                                // en cas de collision avec snoopy ou un mur
                                                else if (plateau [i+1][j-1]=='S' || plateau [i+1][j-1]=='P' || plateau [i+1][j-1]=='M' || plateau [i+1][j-1]=='T'|| plateau [i+1][j-1]=='C'|| plateau [i+1][j-1]=='B')
                                            {
                                                ennemis[k].setOrientation(2);
                                                    ennemis[k].setPosx(i-1);
                                                    ennemis[k].setPosy(j+1);
                                                    temp[i][j]='-';
                                                    temp[i-1][j+1]='B';
                                                    if (plateau [i+1][j-1]=='S')
                                                     perso.setPv(perso.getPv()-1);
                                                    
                                            }
                                                else if ( plateau[i+1][j-1]=='B' )
                                                {
                                                    ennemis[k].setOrientation(2);
                                                }
                                                // cas de bloquage
                                                else 
                                                {
                                                    ennemis[k].setPosx(i);
                                                    ennemis[k].setPosy(j);
                                                    ennemis[k].setOrientation(ennemis[k].getOrientation());
                                                    temp[i][j]='B';
                                                }                                                                                      
                                            break;
                                    }
                                }  
                            }                          
                        }
                    }                    
                }
                if(currentgame.isGameover()==true)
                {
                    scheduler.shutdownNow();
                    System.out.println("nom de la sauvegarde");
                }
                for(int i = 0;i<10;i++)
                {
                    
                    for(int j = 0;j<20;j++)
                    {
                        plateau[i][j]=temp[i][j];
                        System.out.print(plateau[i][j]);
                        
                    }
                    
                    System.out.println();
                    
                }
                System.out.print("score actuel: ");
                System.out.println(currentgame.getScore()); 
                System.out.print("timer: ");
                if (currentgame.getCurrentlevel()==1)
                {
                    if (currentgame.getNv1timer()-1==-1)
                    {
                        currentgame.setNv1timer(60);
                        perso.setPv(perso.getPv()-1);
                        
                    }
                    System.out.println(currentgame.getNv1timer()); 
                 currentgame.setNv1timer(currentgame.getNv1timer()-1);
                 
                }
                if (currentgame.getCurrentlevel()==2)
                {
                    if (currentgame.getNv2timer()-1==-1)
                    {
                        currentgame.setNv2timer(60);
                        perso.setPv(perso.getPv()-1);
                        
                    }
                    System.out.println(currentgame.getNv2timer()); 
                 currentgame.setNv2timer(currentgame.getNv2timer()-1);
                }
                if (currentgame.getCurrentlevel()==3)
                {
                    if (currentgame.getNv3timer()-1==-1)
                    {
                        currentgame.setNv3timer(60);
                        perso.setPv(perso.getPv()-1);
                        
                    }
                    System.out.println(currentgame.getNv3timer()); 
                 currentgame.setNv3timer(currentgame.getNv3timer()-1);
                }
                if (perso.getPv()==0)
                {
                    perso.setFin(1);
                }
                System.out.print("pv restants : "); 
                System.out.print(perso.getPv()); 
                System.out.println();
                
        if (perso.getFin()==1) 
       {
           scheduler.shutdownNow();
       }
            
        };
            }
        };
         final ScheduledFuture<?> affichageHandle =
            scheduler.scheduleAtFixedRate(affichage, 1, 1, SECONDS);
        scheduler.schedule(new Runnable()   {
                                                public void run()   {
                                                affichageHandle.cancel(true); 
                                                                    }
                                            }, 60*60, SECONDS);
        
    }
    
}
