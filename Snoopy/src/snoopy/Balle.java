/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snoopy;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 *
 * @author Richard
 */
public class Balle extends Entity implements Serializable{
    private int nbre_de_balles;
    /*
    orientation : permet de définir le mouvement de la balle : 
    1: bas droite
    2: haut droite
    3: haut gauche
    4: bas gauche
    */
    
    private int orientation;
    private final transient ScheduledExecutorService scheduler =
       Executors.newScheduledThreadPool(1);
    /**
     * Constructeur héritant de Entity
     */
    public Balle()
    {
        super();
    }
    /**
     * Constructeur surchargé
     * @param tx posx de la balle
     * @param ty posy de la balle
     * @param tori orientation de la balle
     */
     public Balle(int tx, int ty,int tori)
    {
        super(tx,ty);
        this.orientation=tori;
    }
    /**
     * @return le nombre de balles
     */
    public int getNbre_de_balles() {
        return nbre_de_balles;
    }

    /**
     * Défini un nombre de balles
     * @param nbre_de_balles le nombre de balles qu'il y aura
     */
    public void setNbre_de_balles(int nbre_de_balles) {
        
        this.nbre_de_balles = nbre_de_balles;
        
    }
    /**
     * Créer un tableau de balles
     * @param tab_de_balles tableau de balles
     * @return le tableau de balles
     */
     public Balle[] creation_balles(Balle [] tab_de_balles)
     {
         
         for (int i=0;i<tab_de_balles.length;i++)
                    {
                     tab_de_balles[i].setPosx((int) (Math.random()*(10)+1));
                     tab_de_balles[i].setPosy((int) (Math.random()*(20)+1));
                    }
        return tab_de_balles;
        
     }
     /**
      * Déplacement automatique de balles toutes les X secondes
      * @param tab tableau de balles
      */
     public void deplacement_auto (Balle[] tab)
     {
                    for (int i = 0; i < tab.length; ++i) 
                   {
                      // System.out.println(tab[i].getPosx()+1);
                       tab[i].setPosx(tab[i].getPosx()+1);
                       tab[i].setPosy(tab[i].getPosy()+1);
                   }
     }

    /**
     * @return the orientation
     */
    public int getOrientation() {
        return orientation;
    }

    /**
     * @param orientation the orientation to set
     */
    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }
    
    
}

