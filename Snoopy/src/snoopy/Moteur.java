/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snoopy;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Richard
 */
public class Moteur implements Runnable {
    Snoopy perso_principal;
    public String nom;
    char [][]plateau;
    Scanner sc = new Scanner(System.in);
    char quit='a';
    Balle [] ennemis;
    Partie currentgame;
    Menu menu1 = new Menu();
    
    /**
     * Constructeur
     * @param tplateau envoie un niveau 
     * @param tperso envoie un personnage Snoopy
     * @param tennemis envoie un tableau de balles
     * @param tcurrentgame envoie une partie
     */
    public Moteur(char [][]tplateau, Snoopy tperso,Balle []tennemis,Partie tcurrentgame)
    {
        nom="t2";
        this.plateau=tplateau;
        this.perso_principal=tperso;
        this.ennemis=tennemis;
        this.currentgame=tcurrentgame;
    }
 
    /**
     * Saisie de l'utlisateur et changement de niveau
     */
    public void run()
    {
        char quit = 0;
        int current_level=0;
        Graphisme graphisme1=new Graphisme();
        char [][] niveau2 = null;
        try {
            niveau2 =graphisme1.recupNiveau(2);
        } catch (IOException ex) {
            Logger.getLogger(Moteur.class.getName()).log(Level.SEVERE, null, ex);
        }
         char [][] niveau3 = null;
        try {
            niveau3 =graphisme1.recupNiveau(3);
        } catch (IOException ex) {
            Logger.getLogger(Moteur.class.getName()).log(Level.SEVERE, null, ex);
        }
        Partie game=new Partie (this.plateau,false,60,0,0,1,this.perso_principal,this.ennemis,0);
      
        String savename;
    ObjectOutputStream oos;
        while (this.currentgame.isGameover()==false)
         {
             //plateau[bloc.getPosx()][bloc.getPosy()]='C';
             this.plateau[perso_principal.getPosx()][perso_principal.getPosy()]='S';
             
                for (int i=0;i<this.ennemis.length;i++)
                {
                       // this.ennemis[0].setPosx(this.ennemis[0].getPosx()+1);
                this.plateau[this.ennemis[i].getPosx()][this.ennemis[i].getPosy()]='B';
                //System.out.println(this.ennemis[i].getPosx()+1);
                
                }
             //System.out.println("saisir" + perso_principal.getPosx() + "   " + perso_principal.getPosy());
             quit=(char)sc.nextLine().charAt(0);
             if (quit=='t')this.currentgame.setGameover(true);
            if (quit=='w')
             {
                 this.currentgame.setGameover(true);
                 Partie sauvegarde=this.currentgame;
                 sauvegarde.setCurrentperso(this.perso_principal);
                 sauvegarde.setEnnemis(this.ennemis);
                 sauvegarde.setPv(this.perso_principal.getPv());
                 System.out.println("nom de la sauvegarde");
                savename=sc.nextLine();
                 File fichier =  new File(savename+".txt") ;
                 try {
                     
                     oos = new ObjectOutputStream(new FileOutputStream(fichier));
                     oos.writeObject(sauvegarde) ;
                     oos.close();
                    } catch (FileNotFoundException ex) {
                     Logger.getLogger(Moteur.class.getName()).log(Level.SEVERE, null, ex);
                 } catch (IOException ex) {
                     Logger.getLogger(Moteur.class.getName()).log(Level.SEVERE, null, ex);
                 } 
                 Thread.currentThread().interrupt();
             }
            if (quit=='p')
            {
                
                /*if (this.plateau[0][0]!='A')this.plateau[0][0]='A';
                else this.plateau[0][0]='#';*/
               if (!this.currentgame.getPause())
                   this.currentgame.setPause(true);
               else this.currentgame.setPause (false);
            }
             perso_principal.deplacer(quit,plateau,perso_principal);
           //if (perso_principal.getNiveau()==4 && current_level==0)
           if(perso_principal.getNiveau()==4 && perso_principal.getLevel()==0)  
           {
                  perso_principal.setNiveau(0);
                  System.out.println("Niveau 1 terminé");
                  this.ennemis[0].setPosx(1);
                  this.ennemis[0].setPosy(7);
                  this.ennemis[0].setOrientation(4);
                  this.ennemis[1].setPosx(8);
                  this.ennemis[1].setPosy(15);
                  this.ennemis[1].setOrientation(2);
                  perso_principal.setLevel(1);
                                       
             }  
              if (current_level ==1 || perso_principal.getLevel()==1)
              {
                  perso_principal.setPosx(6);
                  perso_principal.setPosy(5);
                  this.currentgame.setScore(this.currentgame.getNv1timer());
                 for(int i = 0;i<10;i++)
                {
                    for(int j = 0;j<20;j++)
                    {
                        this.plateau[i][j]=niveau2[i][j];
                    }
                } 
                 this.currentgame.setCurrentlevel(2);
                 perso_principal.setNombre(2);
                 current_level=2;
                 perso_principal.setLevel(2);
              }
             if (perso_principal.getLevel()==2 && perso_principal.getNiveau()==4)
              {
                    System.out.println("Niveau 2 terminé");
                    //current_level++;
                    this.ennemis[0].setPosx(8);
                  this.ennemis[0].setPosy(3);
                  this.ennemis[0].setOrientation(3);
                  this.ennemis[1].setPosx(8);
                  this.ennemis[1].setPosy(15);
                  this.ennemis[1].setOrientation(2);
                    perso_principal.setLevel(3);
                    perso_principal.setNiveau(0);
                }
              if (current_level==3 || perso_principal.getLevel()==3)
                {
                    perso_principal.setPosx(5);;
                    perso_principal.setPosy(7);
                    for(int i = 0;i<10;i++)
                   {
                       for(int j = 0;j<20;j++)
                       {
                           this.plateau[i][j]=niveau3[i][j];  
                       }
                   }
                    this.currentgame.setScore(this.currentgame.getScore()+this.currentgame.getNv2timer());
                    current_level=4;
                    this.currentgame.setCurrentlevel(3);
                    perso_principal.setLevel(4);
                }
             //if ((current_level==4 && perso_principal.getNiveau()==4) || perso_principal.getPv()==0)
             if ((perso_principal.getLevel()==4 && perso_principal.getNiveau()==4) || perso_principal.getPv()==0)
                {
                    this.plateau[perso_principal.getPosx()][perso_principal.getPosy()]='S';
                    perso_principal.setNiveau(0);
                    current_level=0;
                    perso_principal.setFin(1);
                    this.currentgame.setScore(this.currentgame.getScore()+this.currentgame.getNv3timer());
                    
                    
                 try {
                     Thread.sleep(1000);
                 } catch (InterruptedException ex) {
                     Logger.getLogger(Moteur.class.getName()).log(Level.SEVERE, null, ex);
                 }
                 //Thread.currentThread().interrupt();
                 System.out.println("Score : " + this.currentgame.getScore());
                 try {
                     menu1.accueil();
                 } catch (IOException ex) {
                     Logger.getLogger(Moteur.class.getName()).log(Level.SEVERE, null, ex);
                 } catch (ClassNotFoundException ex) {
                     Logger.getLogger(Moteur.class.getName()).log(Level.SEVERE, null, ex);
                 }
                 Thread.currentThread().stop();
                }

            /*  if (perso_principal.getNiveau()==8)
             {
                 for(int i = 0;i<10;i++)
                {
                    for(int j = 0;j<20;j++)
                    {
                        this.plateau[i][j]=niveau3[i][j];
                        this.currentgame.setCurrentlevel(3);
                    }
                }                                 
             } */
             /* if (this.perso_principal.getPv()==3)
              {
                  for(int i = 0;i<1;i++)
                {
                    for(int j = 0;j<20;j++)
                    {
                        this.plateau[i][j]='A';
                         
                        
                    }
                }  
              } 
              if (this.perso_principal.getPv()==2)
              {
                  for(int i = 0;i<1;i++)
                {
                    for(int j = 0;j<20;j++)
                    {
                        this.plateau[i][j]='B';
                        
                    }
                }  
              } 
               if (this.perso_principal.getPv()==1)
              {
                  for(int i = 0;i<1;i++)
                {
                    for(int j = 0;j<20;j++)
                    {
                        this.plateau[i][j]='C';
                        
                    }
                }  
              }*/
         }
        
    }
}
   
